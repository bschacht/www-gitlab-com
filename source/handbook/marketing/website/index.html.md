---
layout: markdown_page
title: "Website Handbook"
---

## Objectives

Generate MQLs by:

1. Showcasing the benefits of the most important GitLab features and how they 
   can save time and money.
2. Compare GitLab vs competing products.
3. Provide customer case studies which illustrate 1 and 2.

## Scope

When referring to the GitLab website, the documentation `docs.gitlab.com` and
the `about.gitlab.com/handbook` are not included.

## Ownership and responsibilities

### Website Product Manager

[Régis Freyd](https://about.gitlab.com/team/#djaiss) is responsible for 
scheduling tasks and allocating various team members to accomplish tasks.

Please insert link to Website Issue Board here.

### Product Marketing

- Value proposition pages like homepage, features, EE standalone, and EE premium 
  feature
- Top of funnel webpages that are focused on lead generation get people 
  interested and excited about GitLab's products (e.g. solution landing pages 
  like GitLab for DevOps or Why Git and GitLab over legacy tools)
- SEO webpages (these are the standalone webpages that Mitchell usually creates)
- Sale enablement pages e.g. /products page, comparison pages, eventual case 
  study page
- Along with the Website Product Manager, create design briefs for the Design 
  team to turn into mockups and designs. Work with the Design team to improve 
  and implement the design brief over a number of iterations. 

### Design team

- Create mockups and designs for design briefs given by the Website Product 
  Manager and Product Marketing team.
- Iterate on the mockups and designs until the Website Product Manger and the 
  Product Marketing team approve.
- Produce a final approved PSD for the Frontend Development team to implement.

### Frontend Development team

- Implement final approved designs.

### All Product Managers 

- Updating the *technical feature comparison tables* on 
  `about.gitlab.com/comparison` and `about.gitlab.com/products` for the products
  they manage e.g GitLab column and competitor columns with list of feature 
  names.

### The Technical Writing team

- Technical content on the `about.gitlab.com/installation` page (not the design 
  and UX of this page which shoud be shared with the marketing site).
- Assist the Product Managers with the backlog of missing *technical feature 
  comparison tables* on `about.gitlab.com/comparison` and 
  `about.gitlab.com/products` e.g GitLab CI vs Jenkins. 
  The Technical Writing team own the tasks from the backlog but will still have 
  to ask the relevant Product Manger for content input as they know the 
  feature's competitive landscape better than anyone.
- With regards to documentation the Technical Writing team is focusing on 
  up to date and feature complete written documentation. No video content is 
  planned for now.
